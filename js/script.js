// Custom script

// Show menu in the top position
// $(window).on('load', function(){
//   if ($(document).width() > 767) {
//     $('.affix-top #navbar-collapse-1').collapse('show');
//   };
// });

// Owl Carousel
$(document).ready(function(){
  if ( $('.owl-carousel').length ) {
    $('.owl-carousel').owlCarousel({
      loop:true,
      rtl:true,
      items:1,
      itemElement:'slide',
      autoplay:true
    });
  }
});


// Top navigation handling
$('.collapse').on('show.bs.collapse shown.bs.collapse', function() {
  $(this).siblings('.in, .collapsing').collapse('hide');
});

$('.site-nav').affix({
  offset: {
    top: 30
  }
});

$('.site-nav').on('affix.bs.affix', function() {
  $('.collapse').collapse('hide');
});


// validation
$.validator.addMethod( "phone", function( phone_number, element ) {
  phone_number = phone_number.replace( /\s+/g, "" );
  return this.optional( element ) || phone_number.length > 9 &&
    phone_number.match( /(?:\(?\+\d{2}\)?\s*)?\d+(?:[ -]*\d+)*$/ );
}, "Please specify a valid phone number" );

$(".menu-form").validate({
  // focusInvalid: false,
  rules: {
    menu_name: {
      required: true
    },
    menu_email: {
      email: true,
      required: true
    },
    menu_tel: {
      phone: true,
      required: true
    },
    menu_subject: {
      required: true
    }
  },
  messages: {
    menu_name: "נא הזן שם מלא",
    menu_email: 'נא למלא כתובת דוא"ל חוקית',
    menu_tel: "נא הזן מספר טלפון",
    menu_subject: "נא בחר פרויקט"
  }
});

$(".main-form").validate({
  // focusInvalid: false,
  rules: {
    main_name: {
      required: true
    },
    main_email: {
      email: true,
      required: true
    },
    main_tel: {
      phone: true,
      required: true
    },
    main_subject: {
      required: true
    }
  },
  messages: {
    main_name: "נא הזן שם מלא",
    main_email: 'נא למלא כתובת דוא"ל חוקית',
    main_tel: "נא הזן מספר טלפון",
    main_subject: "נא בחר פרויקט"
  }
});

$(".cpage-form").validate({
  // focusInvalid: false,
  rules: {
    cpage_name: {
      required: true
    },
    cpage_email: {
      email: true,
      required: true
    },
    cpage_tel: {
      phone: true,
      required: true
    },
    cpage_subject: {
      required: true
    }
  },
  messages: {
    cpage_name: "נא הזן שם מלא",
    cpage_email: 'נא למלא כתובת דוא"ל חוקית',
    cpage_tel: "נא הזן מספר טלפון",
    cpage_subject: "נא בחר פרויקט"
  }
});


// Counters
// checking viewport
$(document).ready(function(){
  if ($('.homepage-counters-block').length ) {
    var counterStarted = false;

    $(window).on('scroll resize', function(){
      
      var countersTopPosition = $('.homepage-counters-block').offset().top;
      var countersHeight = $('.homepage-counters-block').outerHeight();
      var countersBottomPosition = countersTopPosition + countersHeight;
      var windowHeight = $(window).height();
      var documentHeight = $(document).height();
      if ($(this).scrollTop() > countersTopPosition - windowHeight && 
          $(this).scrollTop() <= countersBottomPosition &&
          counterStarted === false) {
        counterStarted = true;
        $('.counter-value').countTo('start');
      };
      if (($(this).scrollTop() > countersBottomPosition || 
          $(this).scrollTop() <= countersTopPosition - windowHeight) &&
          counterStarted === true) {
        counterStarted = false;
        $('.counter-value').countTo('restart');
        $('.counter-value').countTo('stop');
      };
    });
  }
});

// Google Map
function initMap() {
  var myLatlng = new google.maps.LatLng(32.0982772, 34.8619089);
  var mapOptions = {
    zoom: 17,
    center: myLatlng,
    scrollwheel: false,
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false
  };

  var map = new google.maps.Map(document.getElementById("contacts-map"),
      mapOptions);

  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: 'אפלי-קום אלקטרוניקה בע"מ'
  });

  // Map center on resize

  google.maps.event.addDomListener(window, 'resize', function() {
      map.panTo(myLatlng);
  });
};

// Filtering input
function validationForm() {
  $("input[type=email]").on("keypress input", function(a) {
    if ("input" == a.type) $(this).val(function(a, b) {
      return b.replace(/[^\a-z0-9.@_-]/g, "")
    });
    else if (!String.fromCharCode(a.which).match(/^[a-z0-9]+$/i) && 46 != a.which && 64 != a.which && 95 != a.which && 45 != a.which && 32 != a.which && 8 != a.which && 0 !== a.which) return !1
  }), $("input[type=tel]").on("keypress input", function(a) {
    if ("input" == a.type) $(this).val(function(a, b) {
      return b.replace(/[^0-9]/g, "")
    });
    else if (!String.fromCharCode(a.which).match(/^[0-9]+$/i) && 32 != a.which && 8 != a.which && 0 !== a.which) return console.log(a.which), !1
  }), $("input[type=text]").on("keypress input", function(a) {
    if ("input" == a.type) $(this).val(function(a, b) {
      return b.replace(/[^(\u0590-\u05FF)|(a-zA-Z)]/g, "")
    });
    else if (!String.fromCharCode(a.which).match(/[\u0590-\u05FF]/g) && !String.fromCharCode(a.which).match(/[a-zA-Z]/) && 32 != a.which && 8 != a.which && 0 !== a.which) return !1
  });
}
validationForm();
